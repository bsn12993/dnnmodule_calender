# DNNModule_Calender

# Modulo Calendario 

##Clonación

###### Es importante mencionar que cuando se clone el proyecto la ubicación sea dentro del sitio DNN dentro la carpeta **DesktopModules\MVC**.
###### Es importante que sea dentro del sitio del DNN ya que necesita referencias de DLL del proyecto general DNN, a continuación se mencionan las DLL requeridas
- ** DotNetNuke.dll **
- ** DotNetNuke.Web.dll **
- ** DotNetNuke.Web.Mvc.dll **
- ** Microsoft.ApplicationBlocks.Data **
- ** System.Web.Http.dll **

##Conexión a Base de datos Local/Servidor

###### Se puede modificar los datos de la conexión a la base de datos accediendo a los siguientes archivos.
- DAL/DataAccess.cs: la variable **connectionString** contiene la conexión a la base de datos, bastaria con editarla con los datos correspondientes.
	- En esta clase se encuentra los metodos que realizan las consultas a la tabla, por medio de **ADO.NET**.
- Views/CheckIn/Index.cshtml: 
	- **/tasinet2/desktopmodules/CalendarModule/API/ModuleTask/GetData**: Obtener los eventos.
	- **/tasinet2/desktopmodules/CalendarModule/API/ModuleTask/GetEventsType** Obtener tipos de eventos.
		-Para el servidor es colocar al inicio /tasinet2/ de manera local solo basta quitarsela.
	

##Creación de tabla del modulo en DNN
 
###### Si se desea editar el script de la tabla, este se localiza en **Providers\DataProviders\SqlDataProvider** donde se encontraran los siguietes archivos.
- **00.00.01.SqlDataProvider**: este script se ejecuta cuando el modulo se instala, el cual crea la tabla **CREATE TABLE**.
- **Uninstall.SqlDataProvider**: este script se ejecuta cuando el modulo se desinstala el cual elimina la tabla ejecutando **DROP TABLE**.
 
##Peticiónes al Servidor
 
###### En el caso de los modulos desarrollados para DNN las peticiones hacia el Servidor/Controlador se realiza un poco diferente como comunmente se hace en una aplicacion web ASP.NET MVC normal.
###### En este caso la comunicación o petición debe realizarse como un consumo a WebAPI. Los siguienetes archivos estan relacionados a esta configuración.
- **Models/ModuleTaskController.cs**: Es el controlador que contiene los metodos los cuales retornan una respuesta de tipo **HttpResponseMessage** que hereda de **DnnApiController**, ambos metodos usan **HttpPost** para envio de datos al servidor.
- **Models/RouteMapper.cs**: Es la clase donde se configura el mapeo de contralador.
 
 
##Instalación de modulo
###### Para instalar el modulo, antes de hacerlo se debe incluir dentro del archivo **CalenderModule_00.00.01_Install.rar** que se ubica dentro de la carpeta **install** los siguientes archivo:
- ** 
- **
 