﻿var lstEventType = new Array();
var urlServer = "/tasolnet/desktopmodules/CalendarModule/API/ModuleTask/";           

scheduler.locale = {
    date: {
        month_full: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio",
            "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
        month_short: ["Ene", "Feb", "Mar", "Abr", "May", "Jun",
            "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
        day_full: ["Domingo", "Lunes", "Martes", "Miercoles",
            "Jueves", "Viernes", "Sabado"],
        day_short: ["Do", "Lu", "Mar", "Mie", "Jue", "Vie", "Sab"]
    },
    labels: {
        dhx_cal_today_button: "Hoy",
        day_tab: "Dia",
        week_tab: "Semana",
        month_tab: "Mes",
        new_event: "Nuevo  Evento",
        icon_save: "Guardar",
        icon_cancel: "Cancelar",
        icon_details: "Detalles",
        icon_edit: "Editar",
        icon_delete: "Eliminar",
        confirm_closing: "Tus cambios se perderan, ¿estas seguro?", //Your changes will be lost, are your sure?
        confirm_deleting: "El evento será eliminado permanentemente, ¿estas seguro?",
        section_description: "Descripción",
        section_time: "Tiempo periodo"
    }
}

var calender;
function init() {

    var dateNow = new Date();
    var day = dateNow.getDate();
    var month = dateNow.getMonth();
    var year = dateNow.getFullYear();
    var today = dateNow.toISOString().slice(0, 10);
    //calender = new dhtmlXCalendarObject("scheduler_here");
    //calender.setSensitiveRange(today, null);
    scheduler.init("scheduler_here", new Date(year, month, day), "month");



    $.getJSON(urlServer + "GetData", function (data) {
        if (data.IsSuccess) {
            var items = new Array();
            $.each(data.Result, function (key, val) {
                var start_date = val.start_date.split('T');  
                var s_date = start_date[0].split('-')[1] + "/" + start_date[0].split('-')[2] + "/" + start_date[0].split('-')[0] + " " + start_date[1];
                val.start_date = s_date;
                //console.log(s_date);

                var end_date = val.end_date.split('T');
                var e_date = end_date[0].split('-')[1] + "/" + end_date[0].split('-')[2] + "/" + end_date[0].split('-')[0] + " " + end_date[1];
                val.end_date = e_date;
                //console.log(e_date);

                items.push("{  id_Event: '" + val.id_Event + "', text: '" + val.text + "', start_date:'" + val.start_date + "', end_date:'" + val.end_date + "', id_EventType:'" + val.id_eventType + "'}");
            });
            var e = "[" + items.toString() + "]";
            console.log(items.toString());

            scheduler.parse(e, "json");

            scheduler.templates.event_class = function (start, end, event) {
                // id_EventType = 1 -> No Laborable
                // id_EventType = 2 -> Laborable
                // id_EventType = 3 -> Medio Día Laborable
                if (event.id_EventType == '1')
                    return "NoLaborable_event";
                else if (event.id_EventType == '2')
                    return "Laborable_event";
                else if (event.id_EventType == '3')
                    return "MedioDiaLaborable_event";
                else
                    return "Evento_event";
            };

        }
    }).fail(function (x,y,z) {
        console.log("error" + x + "y" + y + "z" + z);
    });

    GetEventType(0);

    scheduler.attachEvent("onEventCreated", function (id, e) {
        var event = scheduler.getEvent(id);
        event.userId = 1;
        return true;
    });

    scheduler.attachEvent('onEventAdded', function (id, event) {
        SetData(urlServer + 'PostData', event);
    });

    scheduler.attachEvent('onEventChanged', function (id, event) {
        SetData(urlServer + 'PutData', event);
    });

    scheduler.attachEvent('onEventDeleted', function (id, event) {
        var event1 = scheduler.getEvent(id);
        SetData(urlServer + 'DeleteData', event);
    });


    scheduler.attachEvent("onClick", function (id, e) {
        //any custom logic here
        var event = scheduler.getEvent(id);
        $.each(scheduler.config.lightbox.sections, function (k, v) {
            if (k == 1) {
                v.default_value = event.id_EventType;
            }
        })
        return true;
    });

}


function SetData(url, data) {
    data.start_date = data.start_date.getFullYear() + "/" + (data.start_date.getMonth() + 1) + "/" + data.start_date.getDate() + " " + data.start_date.getHours() + ":" + data.start_date.getMinutes() + ":" + data.start_date.getSeconds();
    data.end_date = data.end_date.getFullYear() + "/" + (data.end_date.getMonth() + 1) + "/" + data.end_date.getDate() + " " + data.end_date.getHours() + ":" + data.end_date.getMinutes() + ":" + data.end_date.getSeconds();
    $.ajax({
        url: url,
        method: 'POST',
        data: data,
        success: function (result) {
            if (result.IsSuccess) {
                //scheduler.parse(result., "json");
                window.location.reload();
            }
            else {
                alert(result.Message);
                window.location.reload();
            }
        }
    });
}

function GetEventType() {
    lstEventType = new Array();
    debugger;
    var eventType = { key: 0, label: '' };
    $.ajax({
        url: urlServer + 'GetEventsType',
        method: 'POST',
        data: null,
        success: function (result) {
            if (result.IsSuccess) {
                $.each(result.Result, function (k, v) {
                    eventType = { key: 0, label: '' };
                    eventType.key = v.Id_EventType;
                    eventType.label = v.Name;
                    lstEventType.push(eventType);
                });

                scheduler.locale.labels.section_select = 'Tipo Evento';

                scheduler.config.lightbox.sections = [
                    { name: "description", height: 50, map_to: "text", type: "textarea", focus: true },
                    { name: "select", height: 40, map_to: "type_event", type: "select", options: lstEventType },
                    { name: "time", height: 72, type: "time", map_to: "auto" }
                ];
            }
            else {
                alert(result.Message);
            }
        }
    });
}