﻿using CheckInTasiSoft.DAL;
using Christoc.Modules.CalenderModule.Models;
using Christoc.Modules.CalenderModule.Services;
using Christoc.Modules.RegisterModule.Models;
using DHTMLX.Common;
using DotNetNuke.Entities.Users;
using DotNetNuke.Security.Roles;
using DotNetNuke.Web.Api;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace Christoc.Modules.CalenderModule.WebApi
{
    public class ModuleTaskController : DnnApiController
    {
        private DataService DataService = new DataService();
        private Response Response = new Response();

        /// <summary>
        /// Metodo para obtener los registros de los eventos
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet]
        public HttpResponseMessage GetData()
        {
            try
            {
                var data = DataService.GetEvents();
                if (data != null && data.Count > 0) 
                {
                    Response.IsSuccess = true;
                    Response.Message = "Se encontraron datos Calendario";
                    Response.Result = data;
                    return Request.CreateResponse(HttpStatusCode.OK, Response, "application/json");
                }
                else
                {
                    Response.IsSuccess = false;
                    Response.Message = "No se encontraron datos calendario";
                    Response.Result = null;
                    return Request.CreateResponse(HttpStatusCode.OK, Response, "application/json");
                }
            }
            catch(Exception e)
            {
                Response.IsSuccess = false;
                Response.Message = e.Message;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, Response);
            }
        }

        /// <summary>
        /// Metodo para crear evento nuevo
        /// </summary>
        /// <param name="actionValues"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost]
        public HttpResponseMessage PostData(DataRequest actionValues)
        {
            var permistions = DataService.PermitionValidationUsers(UserController.Instance.GetCurrentUserInfo().UserID);
            if (!permistions.IsSuccess)
            {
                return Request.CreateResponse(HttpStatusCode.OK, permistions);
            }

            var data = DataService.InsertEvent(new Event
            {
                text = actionValues.text,
                start_date = actionValues.start_date,
                end_date = actionValues.end_date,
                id_eventType = actionValues.type_event
            });
            if (data.IsSuccess)
            {
                Response.IsSuccess = data.IsSuccess;
                Response.Message = data.Message;
                Response.Result = null;
                return Request.CreateResponse(HttpStatusCode.OK, Response);
            }
            else
            {
                Response.IsSuccess = data.IsSuccess;
                Response.Message = data.Message;
                Response.Result = null;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, Response);
            }
        }

        /// <summary>
        /// Metodo para editar evento
        /// </summary>
        /// <param name="actionValues"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost]
        public HttpResponseMessage PutData(DataRequest actionValues)
        {
            var permistions = DataService.PermitionValidationUsers(UserController.Instance.GetCurrentUserInfo().UserID);
            if (!permistions.IsSuccess)
            {
                return Request.CreateResponse(HttpStatusCode.OK, permistions);
            }

            var data = DataService.UpdateEvent(new Event
            {
                text = actionValues.text,
                start_date = actionValues.start_date,
                end_date = actionValues.end_date,
                id_Event = actionValues.id_Event,
                id_eventType = actionValues.type_event
            });

            if (data.IsSuccess)
            {
                Response.IsSuccess = data.IsSuccess;
                Response.Message = data.Message;
                Response.Result = null;
                return Request.CreateResponse(HttpStatusCode.OK, Response);
            }
            else
            {
                Response.IsSuccess = data.IsSuccess;
                Response.Message = data.Message;
                Response.Result = null;
                return Request.CreateResponse(HttpStatusCode.InternalServerError, Response);
            }
        }

        /// <summary>
        /// Evento para eliminar evento
        /// </summary>
        /// <param name="actionValues"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost]
        public HttpResponseMessage DeleteData(DataRequest actionValues)
        {
            var permistions = DataService.PermitionValidationUsers(UserController.Instance.GetCurrentUserInfo().UserID);
            if (!permistions.IsSuccess)
            {
                return Request.CreateResponse(HttpStatusCode.OK, permistions);
            }

            if (actionValues.id_Event != 0)
            {
                var data = DataService.DeleteEvent(actionValues.id_Event);
                if (data.IsSuccess)
                {
                    Response.IsSuccess = data.IsSuccess;
                    Response.Message = data.Message;
                    Response.Result = null;
                    return Request.CreateResponse(HttpStatusCode.OK, Response);
                }
                else
                {
                    Response.IsSuccess = data.IsSuccess;
                    Response.Message = data.Message;
                    Response.Result = null;
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, Response);
                }
            }
            Response.IsSuccess = false;
            Response.Message = "No se encontro el evento a eliminar";
            return Request.CreateResponse(HttpStatusCode.BadRequest, Response);
        }



        #region Events Type
        [AllowAnonymous]
        [HttpPost]
        public HttpResponseMessage GetEventsType()
        {
            var data = DataService.GetEventsType();
            if(data!=null && data.Count > 0)
            {
                Response.IsSuccess = true;
                Response.Message = "Se encontraron datos";
                Response.Result = data;
            }
            else
            {
                Response.IsSuccess = false;
                Response.Message = "No se encontraron datos";
                Response.Result = null;
            }
            return Request.CreateResponse(HttpStatusCode.OK, Response);
        }
        #endregion
    }
}