﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Christoc.Modules.CalenderModule.Models
{
    public class DataRequest
    {
        public string nativeeditor_status { get; set; }
        public DateTime end_date { get; set; }
        public int id { get; set; }
        public int id_Event { get; set; }
        public DateTime start_date { get; set; }
        public string text { get; set; }
        public int _eday { get; set; }
        public bool _first_chunk { get; set; }
        public bool _last_chunk { get; set; }
        public int _length { get; set; }
        public int _sday { get; set; }
        public int _sorder { get; set; }
        public int _sweek { get; set; }
        public string _text_style { get; set; }
        public bool _timed { get; set; }
        public int type_event { get; set; }

    }
}