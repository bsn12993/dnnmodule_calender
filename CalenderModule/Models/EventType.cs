﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Christoc.Modules.CalenderModule.Models
{
    public class EventType
    {
        public int Id_EventType { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int Status { get; set; }
        public string CreationDate { get; set; }
        public string ModificationDate { get; set; }
        public int Id_userCreated { get; set; }
        public int Id_userModified { get; set; }
    }
}