﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Christoc.Modules.CalenderModule.Models
{
    public class Event
    {
        public int id_Event { get; set; }
        public string text { get; set; }
        public DateTime start_date { get; set; }
        public DateTime end_date { get; set; }
        public int id_eventType { get; set; }
        public string color { get; set; } = string.Empty;
        public string textColor { get; set; } = string.Empty;
    }
}