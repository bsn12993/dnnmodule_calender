﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Christoc.Modules.CalenderModule.Models
{
    public enum EnumEventType
    {
        /// <summary>
        /// Dia Laborable
        /// </summary>
        Laborable = 1,
        /// <summary>
        /// Dia No Laborable
        /// </summary>
        NoLaborable = 2
    }
}