﻿using CheckInTasiSoft.DAL;
using Christoc.Modules.CalenderModule.Models;
using Christoc.Modules.RegisterModule.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Christoc.Modules.CalenderModule.Services
{
    public class DataService
    {
        private DataAccess DataAccess = null;

        public DataService()
        {
            DataAccess = new DataAccess();
        }

        /// <summary>
        /// Metodo que consulta los eventos
        /// </summary>
        /// <returns></returns>
        public List<Event> GetEvents()
        {
            return DataAccess.GetEvents();
        }

        /// <summary>
        /// Metodo que consulta un evento por id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Event GetEventById(int id)
        {
            return DataAccess.GetEventById(id);
        }

        /// <summary>
        /// Metodo que inserta un nuevo evento
        /// </summary>
        /// <param name="evt"></param>
        /// <returns></returns>
        public Response InsertEvent(Event evt)
        {
            return DataAccess.InsertEvent(evt);
        }

        /// <summary>
        /// Metodo que actualiza un evento
        /// </summary>
        /// <param name="evt"></param>
        /// <returns></returns>
        public Response UpdateEvent(Event evt)
        {
            return DataAccess.UpdateEvent(evt);
        }

        /// <summary>
        /// Metodo que elimina un evento por id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Response DeleteEvent(int id)
        {
            return DataAccess.DeleteEvent(id);
        }

        /// <summary>
        /// Metodo que obtiene las opciones del catalogo de tipo de evento
        /// </summary>
        /// <returns></returns>
        public List<EventType> GetEventsType()
        {
            return DataAccess.GetEventsType();
        }

        public Response PermitionValidationUsers(int iduser)
        {
            return DataAccess.PermitionValidationUsers(iduser);
        }
    }
}