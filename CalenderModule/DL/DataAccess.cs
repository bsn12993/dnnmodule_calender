﻿using Christoc.Modules.CalenderModule.BD;
using Christoc.Modules.CalenderModule.Models;
using Christoc.Modules.RegisterModule.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace CheckInTasiSoft.DAL
{
    public class DataAccess
    {
        private SqlParameter[] parameters;
        private string sp = string.Empty;
        private Event Event = null;
        private List<Event> lstEvents = null;
        private EventType EventType = null;
        private List<EventType> lstEventsType = null;
        private Response Response = new Response();
        private ConnectionBD ConnectionBD = new ConnectionBD();
        private string connectionString = string.Empty;

        public DataAccess()
        {
            connectionString = ConnectionBD.GetConnectionLocalString(ConnectionBD.ServerEnvionment);
        }

        #region Events

        /// <summary>
        /// Metodo que consulta los eventos registrados
        /// </summary>
        /// <returns></returns>
        public List<Event> GetEvents()
        {
            sp = @"SELECT id_event
                      ,text
                      ,start_date
                      ,end_date
                      ,id_eventType
                  FROM chk_Events";
            try
            {
                using(var connection=new SqlConnection(connectionString))
                {
                    using(var cmd=new SqlCommand(sp, connection))
                    {
                        cmd.CommandType = System.Data.CommandType.Text;
                        connection.Open();
                        var reader = cmd.ExecuteReader();
                        if (reader.HasRows)
                        {
                            lstEvents = new List<Event>();
                            while (reader.Read())
                            {
                                Event = new Event();
                                Event.id_Event = (int)reader["id_event"];
                                Event.text = (reader["text"] != DBNull.Value) ? (string)reader["text"] : string.Empty;
                                Event.start_date = (DateTime)reader["start_date"];
                                Event.end_date = (DateTime)reader["end_date"];
                                Event.id_eventType = (int)reader["id_eventType"];
                                // id_eventType = 1 -> No Laborable
                                // id_eventType = 2 -> Laborable
                                Event.color = ((int)reader["id_eventType"] == 1) ? "red" : "blue";
                                lstEvents.Add(Event);
                            }
                            return lstEvents;
                        }
                        else
                        {
                            return lstEvents;
                        }
                    }
                }
            }
            catch(Exception e)
            {
                var a = e.Message;
                return null;
            }            
        }

        /// <summary>
        /// Metodo que consulta un evento por su identificador
        /// </summary>
        /// <param name="id_event">identificador del evento</param>
        /// <returns></returns>
        public Event GetEventById(int id_event)
        {
            parameters = new SqlParameter[]
            {
                new SqlParameter("@id_event",id_event)
            };
            sp = @"SELECT id_event
                      ,text
                      ,start_date
                      ,end_date
                      ,id_eventType
                  FROM chk_Events WHERE id_event=@id_event";
            try
            {
                using (var connection = new SqlConnection(connectionString))
                {
                    using (var cmd = new SqlCommand(sp, connection))
                    {
                        cmd.CommandType = System.Data.CommandType.Text;
                        cmd.Parameters.AddRange(parameters);
                        connection.Open();
                        var reader = cmd.ExecuteReader();
                        if (reader.HasRows)
                        {
                            Event = new Event();
                            while (reader.Read())
                            {
                                Event.id_Event = (int)reader["id_event"];
                                Event.text = (reader["text"] != DBNull.Value) ? (string)reader["text"] : string.Empty;
                                Event.start_date = (DateTime)reader["start_date"];
                                Event.end_date = (DateTime)reader["end_date"];
                                Event.id_eventType = (int)reader["id_eventType"];
                            }
                            return Event;
                        }
                        else
                        {
                            return null;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                var a = e.Message;
                return null;
            }
        }

        /// <summary>
        /// Metodo que registra un nuevo evento
        /// </summary>
        /// <param name="evt"></param>
        /// <returns></returns>
        public Response InsertEvent(Event evt)
        {
            parameters = new SqlParameter[]
            {
                new SqlParameter("@text",evt.text),
                new SqlParameter("@start_date",evt.start_date),
                new SqlParameter("@end_date",evt.end_date),
                new SqlParameter("@id_eventType",evt.id_eventType)
            };
            sp = @"INSERT INTO chk_Events
                       (text
                       ,start_date
                       ,end_date
                       ,id_eventType)
                 VALUES
                       (@text
                       ,@start_date
                       ,@end_date
                       ,@id_eventType)";
            try
            {
                using (var connection = new SqlConnection(connectionString))
                {
                    using (var cmd = new SqlCommand(sp, connection))
                    {
                        cmd.CommandType = System.Data.CommandType.Text;
                        cmd.Parameters.AddRange(parameters);
                        connection.Open();
                        var reader = cmd.ExecuteNonQuery();
                        if (reader>0)
                        {
                            Response.IsSuccess = true;
                            Response.Message = "Se ha registrado el evento";
                        }
                        else
                        {
                            Response.IsSuccess = false;
                            Response.Message = "No se pudo registrar el evento";
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Response.IsSuccess = false;
                Response.Message = e.Message;
            }
            return Response;
        }

        /// <summary>
        /// Metodo que actualiza un evento existente
        /// </summary>
        /// <param name="evt"></param>
        /// <returns></returns>
        public Response UpdateEvent(Event evt)
        {
            parameters = new SqlParameter[]
            {
                new SqlParameter("@id_event",evt.id_Event),
                new SqlParameter("@text",evt.text),
                new SqlParameter("@start_date",evt.start_date),
                new SqlParameter("@end_date",evt.end_date),
                new SqlParameter("@id_eventType",evt.id_eventType)
            };
            sp = @"UPDATE chk_Events
                   SET text = @text
                      ,start_date = @start_date
                      ,end_date = @end_date
                      ,id_eventType=@id_eventType
                 WHERE id_event=@id_event";
            try
            {
                using (var connection = new SqlConnection(connectionString))
                {
                    using (var cmd = new SqlCommand(sp, connection))
                    {
                        cmd.CommandType = System.Data.CommandType.Text;
                        cmd.Parameters.AddRange(parameters);
                        connection.Open();
                        var reader = cmd.ExecuteNonQuery();
                        if (reader > 0)
                        {
                            Response.IsSuccess = true;
                            Response.Message = "Se ha actualizado el evento";
                        }
                        else
                        {
                            Response.IsSuccess = false;
                            Response.Message = "No se pudo actualizar el evento";
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Response.IsSuccess = false;
                Response.Message = e.Message;
            }
            return Response;
        }

        /// <summary>
        /// Metodo que elimina un evento por su identificador
        /// </summary>
        /// <param name="id_event">identificador del evento</param>
        /// <returns></returns>
        public Response DeleteEvent(int id_event)
        {
            parameters = new SqlParameter[]
            {
                new SqlParameter("@id_event",id_event)
            };
            sp = @"DELETE FROM chk_Events
                    WHERE id_event=@id_event";
            try
            {
                using (var connection = new SqlConnection(connectionString))
                {
                    using (var cmd = new SqlCommand(sp, connection))
                    {
                        cmd.CommandType = System.Data.CommandType.Text;
                        cmd.Parameters.AddRange(parameters);
                        connection.Open();
                        var reader = cmd.ExecuteNonQuery();
                        if (reader > 0)
                        {
                            Response.IsSuccess = true;
                            Response.Message = "Se ha eliminado el evento";
                        }
                        else
                        {
                            Response.IsSuccess = false;
                            Response.Message = "No se pudo eliminar el evento";
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Response.IsSuccess = false;
                Response.Message = e.Message;
            }
            return Response;
        }
        #endregion

        #region Events Type
        /// <summary>
        /// Metodo que consulta las opciones del catalogo de los tipos de eventos
        /// </summary>
        /// <returns></returns>
        public List<EventType> GetEventsType()
        {
            sp = @"SELECT id_eventType
                      ,name
                      ,description
                      ,status
                      ,creationDate
                      ,modificationDate
                      ,id_userCreated
                      ,id_userModified
                  FROM chk_EventType";
            try
            {
                using (var connection = new SqlConnection(connectionString))
                {
                    using (var cmd = new SqlCommand(sp, connection))
                    {
                        cmd.CommandType = System.Data.CommandType.Text;
                        connection.Open();
                        var reader = cmd.ExecuteReader();
                        if (reader.HasRows)
                        {
                            lstEventsType = new List<EventType>();
                            while (reader.Read())
                            {
                                EventType = new EventType();
                                EventType.Id_EventType = (int)reader["id_eventType"];
                                EventType.Name = (string)reader["name"];
                                EventType.Description = (string)reader["description"];
                                EventType.Status = (int)reader["status"];
                                lstEventsType.Add(EventType);
                            }
                            return lstEventsType;
                        }
                        else
                        {
                            return lstEventsType;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                var a = e.Message;
                return null;
            }
        }
        #endregion

        public Response PermitionValidationUsers(int iduser)
        {
            parameters = new SqlParameter[]
            {
                new SqlParameter("@UserID",iduser)
            };
            sp = @"SELECT r.RoleName
                    FROM   UserRoles ur
                    inner join Roles r on ur.RoleID=r.RoleID 
                    WHERE ur.UserID=@UserID
                    and ur.RoleID in ((SELECT RoleID FROM dbo.Roles 
                    WHERE 
                    RoleName in ('Administrators','Soporte_Tecnico','Soporte_RH')))";
            try
            {
                using (var connection = new SqlConnection(connectionString))
                {
                    using (var cmd = new SqlCommand(sp, connection))
                    {
                        cmd.CommandType = System.Data.CommandType.Text;
                        cmd.Parameters.AddRange(parameters);
                        connection.Open();
                        var reader = cmd.ExecuteReader();
                        if (reader.HasRows)
                        {
                            return new Response
                            {
                                IsSuccess = true,
                                Message = "Tiene permisos para realizar esta acción",
                                Result = null
                            };
                        }
                        else
                        {
                            return new Response
                            {
                                IsSuccess = false,
                                Message = "No tiene permisos para realizar esta acción",
                                Result = null
                            };
                        }
                    }
                }
            }
            catch (Exception e)
            {
                var a = e.Message;
                return new Response
                {
                    IsSuccess = false,
                    Message = e.Message,
                    Result = null
                };
            }
        }
    }
}
 